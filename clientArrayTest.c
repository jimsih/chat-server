#include <stdio.h>

#include "clientArray.h"
#include "array.h"

void test1() {
	array *arr = array_create(4);

	client *c1 = malloc(sizeof(client));
	client *c2 = malloc(sizeof(client));
	client *c3 = malloc(sizeof(client));

	char c1nick[256] = "derp\0";
	char c2nick[256] = "derp\0";
	char c3nick[256] = "derp\0";

	printf("TEST: 3 clients joining with nickname derp \n");
	printf("Comment: Clients should not have same nickname \n");

	clients_setNick(arr, c1, c1nick);
	clients_add(arr, c1);

	clients_setNick(arr, c2, c2nick);
	clients_add(arr, c2);

	clients_setNick(arr, c3, c3nick);
	clients_add(arr, c3);

	printf("client 1: %s \n", c1->nick);
	printf("client 2: %s \n", c2->nick);
	printf("client 3: %s \n", c3->nick);

	clients_remove(arr, c1);
	clients_remove(arr, c2);
	clients_remove(arr, c3);
	array_free(arr);
}

void test2() {
	array *arr = array_create(4);

	client *c1 = malloc(sizeof(client));
	client *c2 = malloc(sizeof(client));
	client *c3 = malloc(sizeof(client));
	client *c4 = malloc(sizeof(client));

	char *c1nick = malloc(256);
	char *c2nick = malloc(256);
	char *c3nick = malloc(256);
	char *c4nick = malloc(256);

	for (int i = 0; i < 255; i++) {
		c1nick[i] = 'a';
	}
	c1nick[255] = '\0';

	for (int j = 0; j < 255; j++) {
		c2nick[j] = 'a';
	}
	c2nick[255] = '\0';

	for (int k = 0; k < 255; k++) {
		c3nick[k] = 'a';
	}
	c3nick[255] = '\0';

	for (int l = 0; l < 255; l++) {
		c4nick[l] = 'a';
	}
	c4nick[255] = '\0';

	printf("TEST: 4 clients joining with same nickname of size 255 \n");
	printf("Comment: Clients should not have same nickname \n");

	clients_setNick(arr, c1, c1nick);
	clients_add(arr, c1);

	clients_setNick(arr, c2, c2nick);
	clients_add(arr, c2);

	clients_setNick(arr, c3, c3nick);
	clients_add(arr, c3);

	clients_setNick(arr, c4, c4nick);
	clients_add(arr, c4);

	printf("client 1: %s \n", c1->nick);
	printf("client 2: %s \n", c2->nick);
	printf("client 3: %s \n", c3->nick);
	printf("client 4: %s \n", c4->nick);

	clients_remove(arr, c1);
	clients_remove(arr, c2);
	clients_remove(arr, c3);
	//clients_remove(arr, c4);
	free(c1nick);
	free(c2nick);
	free(c3nick);
	free(c4nick);
	array_free(arr);
}

int main(void) {

	test1();
	printf("\n\n------------------------\n\n");
	test2();
	return 0;
}

