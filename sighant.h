
#ifndef _SIGHANT_
#define _SIGHANT_

typedef void Sigfunc(int);

Sigfunc* signal(int, Sigfunc *);

/* Handles signal SIGINT and interups all executions */
void signal_handler(int theSignal);


#endif
