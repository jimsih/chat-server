#include "ipConverter.h"

int string2ip(char *s) {
	struct in_addr ip_addr;
	int ip = inet_aton(s, &ip_addr);
	if (ip == 0) {
		fprintf(stderr, "%s", "Error converting host");
	}

	return ntohl(ip_addr.s_addr);
}

char *ip2string(int ip) {
	struct in_addr ip_addr;
	ip_addr.s_addr = ip;
	char *host = inet_ntoa(ip_addr);
	char *h = malloc(sizeof(char) * (strlen(host) + 1));
	strcpy(h, host);
	return h;
}

void ipTest() {
	char* host = "102.23.32.3";
	printf("host: %s\n", host);
	int ip = string2ip(host);
	printf("ip: %d\n", ip);
	char *host2 = ip2string(ip);
	printf("ip: %s\n", host2);
}

char *hostname2string(char* hostname) {
	// This one should not be freed.
	struct hostent *hp = gethostbyname(hostname);
	struct in_addr ip_addr;
	ip_addr = *(struct in_addr *) (hp->h_addr_list[0]);
	char *c = inet_ntoa(ip_addr);
	fprintf(stderr, "%s\n", c);
	return c;
}
/**
 * Get ip from hostname
 */
uint32_t hostname2ip(char* hostname) {
	char *host = hostname2string(hostname);
	uint32_t temp = inet_addr(host);
	return temp;
}

char *
getMyIp() {
	struct ifaddrs *ifaddr, *ifa;
	int family, s;
	char host[NI_MAXHOST];

	if (getifaddrs(&ifaddr) == -1) {
		perror("getifaddrs");
		exit(EXIT_FAILURE);
	}

	/* Walk through linked list, maintaining head pointer so we
	 can free list later */

	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
		if (ifa->ifa_addr == NULL)
			continue;

		family = ifa->ifa_addr->sa_family;
		if (family != 2)
			continue;

		/* Display interface name and family (including symbolic
		 form of the latter for the common families) */

		/* For an AF_INET* interface address, display the address */

		if (family == AF_INET || family == AF_INET6) {
			s = getnameinfo(ifa->ifa_addr,
					(family == AF_INET) ?
							sizeof(struct sockaddr_in) :
							sizeof(struct sockaddr_in6), host, NI_MAXHOST, NULL,
					0, NI_NUMERICHOST);
			if (s != 0) {
				printf("getnameinfo() failed: %s\n", gai_strerror(s));
				exit(EXIT_FAILURE);
			}
		}
	}

	freeifaddrs(ifaddr);
	char *h = calloc(sizeof(char), 1024);
	strcpy(h, host);

	return h;
}
