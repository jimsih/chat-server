################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../chatPDU.o \
../ipConverter.o \
../pdu.o \
../server.o 

C_SRCS += \
../array.c \
../arrayTest.c \
../chatPDU.c \
../chatPDUTest.c \
../clientArray.c \
../clientArrayTest.c \
../ipConverter.c \
../pdu.c \
../server.c \
../timeTest.c \
../udp-sample.c 

OBJS += \
./array.o \
./arrayTest.o \
./chatPDU.o \
./chatPDUTest.o \
./clientArray.o \
./clientArrayTest.o \
./ipConverter.o \
./pdu.o \
./server.o \
./timeTest.o \
./udp-sample.o 

C_DEPS += \
./array.d \
./arrayTest.d \
./chatPDU.d \
./chatPDUTest.d \
./clientArray.d \
./clientArrayTest.d \
./ipConverter.d \
./pdu.d \
./server.d \
./timeTest.d \
./udp-sample.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -std=c99 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


