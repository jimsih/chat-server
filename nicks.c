#include "clientArray.h"

nicknames getAllNicks(array *clients, char *nick_joined) {

	client *c;
	int nicks_size = 0;
	int num = 0;
	// Get length of all nicknames

	for (int i = 0; i < 255; i++) {
		if (clients->d[i] == 0) {
			continue;
		}
		c = clients_get(clients, i);
		nicks_size += (int) strlen(c->nick) + 1; // Add 1 to include \0
		num++; // Increase number of nicknames
	}

	nicknames client_nicks;
	client_nicks.nicks = malloc(nicks_size + strlen(nick_joined) + 1);
	client_nicks.len = nicks_size + strlen(nick_joined) + 1;
	client_nicks.num_nicks = num + 1;
	int count = 0;
	memcpy(client_nicks.nicks + count, nick_joined, strlen(nick_joined) + 1);
	count = strlen(nick_joined) + 1;

	for (int i = 0; i < 255; i++) {
		if (clients->d[i] == 0) {
			continue;
		}
		c = clients_get(clients, i);
		memcpy(client_nicks.nicks + count, c->nick, strlen(c->nick) + 1);
		count += strlen(c->nick);

		//count++; // Add 1 to include \0
		//client_nicks.nicks[count] = '\0';
		count++; // Set counter on next empty byte
	}

	return client_nicks;
}

int main() {
	array *clients = array_create(256);
	client c;
	c.nick = "hejett";

	client d;
	d.nick = "hejtva";

	client e;
		e.nick = "hejtre";



	array_add(clients, 12, (void*) &c);
	array_add(clients, 11, (void*) &d);
	array_add(clients, 10, (void*) &e);

	nicknames client_nicks = getAllNicks(clients, "hejsan\0");

	for (int i = 0; i < client_nicks.len; i++) {
		printf("[%c]", client_nicks.nicks[i]);
	}

}
