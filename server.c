#include "server.h"

array *clients;
pthread_mutex_t clientList_lock;
nameServer *ns;

int running;
int servSock; /* Socket descriptor for server */
int serverPort; /* Server port*/
char *serverName; /* server name */

void DieWithError(char *errorMessage) {
	fprintf(stderr, "Error: %s", errorMessage);
	exit(1);
}

void quitClientWithULEAVEAndMessage(client *c, char* mesg) {
	char nick_tmp[256];
	sendMessageToClient(mesg, c);
	strcpy(nick_tmp, c->nick);
	quitClient(c);
	sendULEAVE(nick_tmp);
}


void quitClient(client *c) {
	PDU *pdu_Q = PDU_QUIT();
	if (send(c->socket, pdu_Q->pdu, pdu_Q->size, MSG_NOSIGNAL) == -1)
		perror("quitClient: ");
	PDU_free(pdu_Q);
	clients_remove(clients, c);
}


/* TCP client handling function */
void *HandleTCPClient(void *arguments) {
	char mesg[MESSAGE_LIMIT];
	int pdu_len = 0;
	int pdu_ptr = 0;
	int clientID;
	//int clientIDLE = 0;

	client *c = (client *)arguments;
	//setSocketTimeout(c->socket, 15);
	c->id = -1;
	mesg[0] = -1;

	// Expecting JOIN as first pdu
	pdu_len = PDU_READ_INCOMING(c->socket, mesg);

	if (pdu_len == -1) {
		printf("failed to join \n");
		quitClient(c);
		return NULL;
	}

	if (mesg[0] == JOIN) {
		clientID = handleJOIN(c, mesg);
		if (clientID == -1) {
			quitClient(c);
			return NULL;
		}
	} else { // Invalid first pdu packet
		sendMessageToClient("Error: Must JOIN first\0", c);
		quitClient(c);
		printf("client kicked, false init pdu \n");
		return NULL;
	}

	while (running) {
		//pdu_len = recv(c->socket, mesg, 1000, 0);
		pdu_len = PDU_READ_INCOMING(c->socket, mesg);

		if (pdu_len == -1) {
			printf("Failed to read pdu \n");
			quitClientWithULEAVEAndMessage(c, "Failed to read message\0");
			return NULL;
		}
//		if (pdu_len == -1) {
//			clientIDLE++;
//			//printf("clientIDLE: %d \n", clientIDLE);
//			if(clientIDLE > 500) {
//				printf("Client %s was kicked, IDLE \n", c->nick);
//				quitClientWithULEAVEAndMessage(c, "Kicked due to IDLE timeout\0");
//				return NULL;
//			}
//			continue;
//		}

		//clientIDLE = 0;
		pdu_ptr = 0;
		switch (mesg[pdu_ptr]) {
		case MESSAGE:
		{
			if (checksum_calc(mesg, pdu_len) == 0) {
				int mess_len = (int)(mesg[4] << 8) | mesg[5];
				PDU *pdu_M = PDU_MESSAGE((int)mesg[1], mesg+12, mess_len, c->nick);
				sendToAll(pdu_M);
				PDU_free(pdu_M);
			} else {
				printf("Client %s had bad checksum \n", c->nick);
				quitClientWithULEAVEAndMessage(c, "Error: Bad checksum\0");
				return NULL;
			}

			break;
		}
		case CHNICK:
			if (handleCHNICK(c, mesg, clientID) == -1) {
				sendMessageToClient("Error: Invalid nickname\0", c);
			}
			break;
		case QUIT:
			printf("client %s left with quit \n", c->nick);
			char nick_tmp[256];
			strcpy(nick_tmp, c->nick);
			quitClient(c);
			sendULEAVE(nick_tmp);
			return NULL;
			break;
		default:
			printf("Wrong op=%d, client %s kicked  \n",mesg[0], c->nick);
			quitClientWithULEAVEAndMessage(c, "Error: wrong OP-code\0");
			return NULL;
			break;
		}
	}
	quitClient(c);
	return NULL;
}

int handleCHNICK(client *c, char *mesg, int clientID) {
	int pdu_ptr = 1; // point to new nick length
	int nick_len = (int)(unsigned char) mesg[pdu_ptr];

	if (nick_len == 0) {
		printf("Trying to take severname \n");
		return -1;
	}
	pdu_ptr += 3; // point to new nick

	char new_nick[256], old_nick[256];

	strcpy(old_nick, c->nick);
	memcpy(new_nick, mesg+pdu_ptr, nick_len);
	new_nick[nick_len] = '\0';
	printf("new nickname: %s \n", new_nick);
	clients_cnick(clients, clientID, new_nick);
	printf("modified nickname: %s \n", c->nick);

	PDU *pdu_UC = PDU_UCNICK(old_nick, c->nick);
	sendToAll(pdu_UC);
	PDU_free(pdu_UC);

	return 1;
}

void sendPDUToClient(PDU *pdu, client *c) {
	if (send(c->socket, pdu->pdu, pdu->size, MSG_NOSIGNAL) == -1)
			perror("SendPDUToClient: ");
}

void sendMessageToClient(char* message, client *c) {
	PDU *pdu_M = PDU_MESSAGE(0, message, strlen(message), "");
	if (send(c->socket, pdu_M->pdu, pdu_M->size, MSG_NOSIGNAL) == -1)
		perror("SendMessageToClient: ");
	free(pdu_M);
}

void sendToAll(PDU *pdu) {
	pthread_mutex_lock(&clientList_lock);
	client *c;
	for (int i=0; i<255; i++) {
		if (clients->d[i] == 0) {
			continue;
		}
		c = clients_get(clients, i);
		if (send(c->socket, pdu->pdu, pdu->size, MSG_NOSIGNAL) == -1)
			perror("Send to all: ");
	}
	pthread_mutex_unlock(&clientList_lock);
}

void sendULEAVE(char *c) {
	PDU *pdu_UL = PDU_ULEAVE(c);
	sendToAll(pdu_UL);
	PDU_free(pdu_UL);
}

int handleJOIN(client *c, char* mesg) {
	char nick[256];
	int pdu_ptr = 1; // point to length of nickname in PDU
	int nick_len = (int)(unsigned char) mesg[pdu_ptr];
	int clientID;

	if (nick_len == 0) {
		sendMessageToClient("Error: Nickname Invalid\0", c);
		printf("Invalid nickname, client kicked when joining\n");
		return -1;
	}
	if (nick_len > NICKNAME_LIMIT)
		nick_len = NICKNAME_LIMIT;

	pdu_ptr += 3; // point to nickname in PDU

	memcpy(nick, mesg+pdu_ptr, nick_len);
	nick[nick_len] = '\0';

	nicknames client_nicks = getAllNicks();
	clientID = clients_add(clients, c, nick);
	printf("%s joined \n", c->nick);

	if (clientID == -1) {
		sendMessageToClient("Error: No slots available\0", c);
		printf("Server full, client kicked when joining\n");
		free(client_nicks.nicks);
		return -1;
	}
	nick_len = strlen(c->nick)+1;
	char nicknames[nick_len+client_nicks.len]; /* Send joined clients nickname first */
	memcpy(nicknames, c->nick, nick_len);
	memcpy(nicknames+nick_len, client_nicks.nicks, client_nicks.len);
	PDU* pdu_N = PDU_NICKS(client_nicks.num_nicks+1, nicknames, client_nicks.len+nick_len);
	sendPDUToClient(pdu_N, c);

	PDU_free(pdu_N);
	free(client_nicks.nicks);

	PDU* pdu_UJ = PDU_UJOIN(c->nick);
	sendToAll(pdu_UJ);
	PDU_free(pdu_UJ);
	return clientID;
}

// Collects all nicknames and puts nick_joined first
// getAllNicks should be used before adding new client to client list
nicknames getAllNicks() {
	client *c;
	int nicks_size = 0;
	int num = 0;
	// Get length of all nicknames
	pthread_mutex_lock(&clientList_lock);
	for (int i=0; i<255; i++) {
		if (clients->d[i] == 0) {
			continue;
		}
		c = clients_get(clients, i);

		nicks_size += (int) strlen(c->nick)+1; // Add 1 to include \0
		num++; // Increase number of nicknames
	}

	nicknames client_nicks;
	client_nicks.nicks = malloc(nicks_size);
	client_nicks.len = nicks_size;
	client_nicks.num_nicks = num;
	int count = 0;

	for (int i=0; i<255; i++) {
		if (clients->d[i] == 0) {
			continue;
		}
		c = clients_get(clients, i);
		memcpy(client_nicks.nicks+count, c->nick, strlen(c->nick)+1);
		count += strlen(c->nick)+1;
	}
	pthread_mutex_unlock(&clientList_lock);

	return client_nicks;
}

void connectToNameServer(nameServer *ns) {

	pthread_attr_t attr;	// thread's attribute
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	// Create nameserver thread
	pthread_t *thread = malloc(sizeof(pthread_t));
	ns->thread = thread;
	pthread_create(thread, &attr, startNameServerConnection, (void *) ns);

}


int main(int argc, char *argv[]) {

	clients = clients_create(256);

	int nameServerPort;
	int clntSock; /* Socket descriptor for client */
	struct sockaddr_in serverSocket; /* Local address */
	struct sockaddr_in echoClntAddr; /* Client address */
	unsigned int clntLen; /* Length of client address data structure */
	char  *nameServer;


	if (argc < 3) /* Test for correct number of arguments */
	{
		fprintf(stderr, "Usage:  %s <Server> <Server Port> [<NameServer> <NameServer Port>]\n", argv[0]);
		exit(1);
	}

	serverName = argv[1];
	serverPort = atoi(argv[2]); /* First arg:  local port */
	nameServer = argv[3];
	nameServerPort = atoi(argv[4]);


	/* creates signalhandler for signal SIGINT */
	if(signal(SIGINT, signal_handler) == SIG_ERR) {
		perror("Couldn't register SIGINT handler ");
	}
	/* creates signalhandler for signal SIGPIPE */
	if(signal(SIGPIPE, signal_handler) == SIG_ERR) {
		perror("Couldn't register SIGPIPE handler ");
	}

	/* Fix more checks on input (valid port number etc)*/

	if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		perror("Server: ");
		DieWithError("socket() failed");
	}


	/* Construct local address structure */
	memset(&serverSocket, 0, sizeof(serverSocket)); /* Zero out structure */
	serverSocket.sin_family = AF_INET; /* Internet address family */
	serverSocket.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
	serverSocket.sin_port = htons(serverPort); /* Local port */

	/* Bind to the local address */
	if (bind(servSock, (struct sockaddr *) &serverSocket, sizeof(serverSocket))
			< 0) {
		perror("Server: ");
		DieWithError("bind() failed");
	}

	/* Mark the socket so it will listen for incoming connections */
	if (listen(servSock, MAXPENDING) < 0) {
		perror("Server: ");
		DieWithError("listen() failed");
	}

	ns = NULL;
	if (argc == 5) {
		ns = initNameServer(nameServer, nameServerPort);
		connectToNameServer(ns);
	}


	pthread_mutex_init(&clientList_lock, NULL);
	running = 1;
	for (;;) /* Run forever */
	{
		/* Set the size of the in-out parameter */
		clntLen = sizeof(echoClntAddr);

		/* Wait for a client to connect */
		if ((clntSock = accept(servSock, (struct sockaddr *) &echoClntAddr,
				&clntLen)) < 0) {
			perror("Server: ");
			continue;
			//DieWithError("accept() failed");
		}

		/* clntSock is connected to a client! */

		printf("Handling client %s\n", inet_ntoa(echoClntAddr.sin_addr));

		pthread_t *thread = malloc(sizeof(pthread_t));
		client *c = malloc(sizeof(client));
		c->socket = clntSock;
		c->sock_addr =  (struct sockaddr *) &echoClntAddr;
		c->sock_len = &clntLen;
		c->thread = thread;

		pthread_attr_t attr;	// thread's attribute
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

		pthread_create(thread, &attr, HandleTCPClient, c);
	}

}
