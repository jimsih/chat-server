#ifndef SERVER_H_
#define SERVER_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>

#include "opCodes.h"
#include "pdu.h"
#include "limits.h"
#include "chatPDU.h"
#include "clientArray.h"
#include "checksum.h"
#include "udp.h"
#include "sighant.h"

#define MAXPENDING 5    /* Maximum outstanding connection requests */



extern nameServer *ns;
extern array *clients;
extern int running;
extern int servSock; /* Socket descriptor for server */
extern int serverPort; /* Server port*/
extern char *serverName;

void DieWithError(char *errorMessage);

void *HandleTCPClient(void *arguments);

int handleCHNICK(client *c, char * mesg, int clientID);

int handleJOIN(client *c, char *mesg);

void sendULEAVE(char *c);

void quitClient(client *c);

void sendMessageToClient(char* message, client *c);

nicknames getAllNicks();

void sendToAll(PDU *pdu);

void connectToNameServer(nameServer *ns);

#endif
