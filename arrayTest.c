#include "array.h"

void array_test() {
	array *a = array_create(2);

	int b = 2;
	int c = 3;

	printf("\n%d = %d", 0, array_get_free_index(a));

	array_add(a, 0, (data) &b);
	printf("\n%d = %d", 1, array_get_free_index(a));

	array_add(a, 1, (data) &c);
	printf("\n%d = %d", 2, array_get_free_index(a));

	array_remove(a, 0);
	printf("\n%d = %d", 0, array_get_free_index(a));

	printf("\n%d = %d", 0, *(int *) array_get(a, 1));

}

int main() {
	array_test();
	return 0;
}
