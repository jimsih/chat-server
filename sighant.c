/* sighant.c
 * This file handles the signal SIGINT
 *
 * Author: Jimmy Sihlberg
 * Date:   2013-10-17
 * Laboration <4> <systemn�ra programmering> <ht13>
 */

#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>

#include "sighant.h"
#include "server.h"

/* function checks for signal SIGINT and
 * kills all children processes when found
 */
void signal_handler(int theSignal) {

	printf("signal: ");

	if (theSignal == SIGPIPE)
		printf("SIGPIPE \n");

	if (theSignal == SIGINT) {
		printf("SIGINT \n");
		running = 0;
		sleep(5);
		close(servSock);
		array_free(clients);
		if (ns != NULL) {
			free(ns->thread);
			free(ns);
		}

		exit(0);
	}


}
