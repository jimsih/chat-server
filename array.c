#include "array.h"

int array_valid_index(array *a, int index) {
	return (index >= 0 && index < a->size);
}

array *array_create(int length) {
	data d = malloc(sizeof(data) * length);
	memset(d, 0, sizeof(data) * length);
	array *a = malloc(length);
	a->d = d;
	a->size = length;
	a->numElements = 0;
	return a;
}

void array_add(array *a, int index, data d) {
	if (array_valid_index(a, index)) {
		a->d[index] = d;
		a->numElements++;
	}

}

data *array_get(array *a, int index) {
	if (array_valid_index(a, index))
		return a->d[index];
	return NULL;
}

void array_remove(array *a, int index) {
	if (array_valid_index(a, index)) {
		a->d[index] = 0;
		a->numElements--;
	}

}

int array_get_free_index(array *a) {
	for (int i = 0; i < a->size; i++)
		if (a->d[i] == NULL)
			return i;
	return -1;
}

void array_free(array *a) {
	free(a->d);
	free(a);
}

