#include "clientArray.h"
#include "array.h"

pthread_mutex_t clientList_lock;

array *clients_create(int length) {
	array *a = array_create(length);
	return a;
}

/* Checks if nickname is already in use in client array
 * If nickname is unavailable setNick will modify nickname
*/
void clients_setNick(array *clis, client *cli, char *nick) {
	if (clients_nick_is_occupied(clis, nick)) {
		modifyNickname(clis, nick);
	}
	cli->nick = malloc(strlen(nick)+1);
	strcpy(cli->nick, nick);
}

// TODO:
void clients_cnick(array *clis, int index, char *newNick) {
	pthread_mutex_lock(&clientList_lock);
	if (clients_nick_is_occupied(clis, newNick)) {
		modifyNickname(clis, newNick);
	}

	client *c = clients_get(clis, index);
	free(c->nick);
	c->nick = malloc(strlen(newNick)+1);
	strcpy(c->nick, newNick);
	pthread_mutex_unlock(&clientList_lock);
}

int clients_nick_is_occupied(array *clis, char *nick) {
	client *c;
	//pthread_mutex_lock(&clientList_lock);
	for (int i = 0; i < clis->size; i++) {
		if (clis->d[i] == 0) {
			continue;
		}
		c = (client *) array_get(clis, i);
		if (strcmp(c->nick, nick) == 0) {
			//pthread_mutex_unlock(&clientList_lock);
			return 1;
		}
	}
	//pthread_mutex_unlock(&clientList_lock);
	return 0;
}
// adds client to list. Returns 0 if list is full, else 1
int clients_add(array *clis, client *cli, char* nick) {
	pthread_mutex_lock(&clientList_lock);
	clients_setNick(clis, cli, nick);
	int i = array_get_free_index(clis);
	if (i == -1) {
		pthread_mutex_unlock(&clientList_lock);
		return i;
	}
	cli->id = i;
	array_add(clis, i, (void *) cli);
	pthread_mutex_unlock(&clientList_lock);
	return i;
}

client* clients_get(array *clis, int index) {
	client *c = (client*) array_get(clis, index);
	return c;
}

void clients_remove(array *clis, client *cli) {
	pthread_mutex_lock(&clientList_lock);
	array_remove(clis, cli->id);
	free(cli->thread);
	if (cli->id != -1) {
		free(cli->nick);
	}
	if (close(cli->socket) == -1)
		perror("Client_remove");

	free(cli);
	pthread_mutex_unlock(&clientList_lock);
}

// Expects nick to be of size 256
void modifyNickname(array *clis, char *nick) {
	if (strlen(nick) < 255) {
		do {
			modifyHelpFunc(nick);
		} while (strlen(nick) < 255 && clients_nick_is_occupied(clis, nick));
		if (strlen(nick) < 255) {
			return;
		}
	}

	int i = 2;
	int start_len = strlen(nick);

	do {
		nick[start_len - i] = '\0';

		while ((start_len != (int)strlen(nick)) && clients_nick_is_occupied(clis, nick)) {
			modifyHelpFunc(nick);
		}

		i++;

	} while (clients_nick_is_occupied(clis, nick));

}

void modifyHelpFunc(char *nick) {
	regex_t regex;
	regmatch_t m[1];

	// Check if nick ends with a number
	if (regcomp(&regex, "[0-9]+$", REG_EXTENDED) != 0) {
		printf("regcomp error \n");
		return;
	}

	if (regexec(&regex, nick, 1, m, 0) == REG_NOMATCH) {
		int len = strlen(nick);
		nick[len] = '1';
		nick[len+1] = '\0';

	} else {
		char s[((int)m[0].rm_eo - (int)m[0].rm_so)+2]; // Make space for new decimal and \0

		memcpy(s, nick+((int)m[0].rm_so), (int)m[0].rm_eo - (int)m[0].rm_so);

		int num = atoi(s);
		num++;
		sprintf(s, "%d", num);
		memcpy(nick+((int)m[0].rm_so), s, strlen(s)+1);
	}

	regfree(&regex);
}
