#ifndef UDP_UDP_H_
#define UDP_UDP_H_

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <ifaddrs.h>

#include "array.h"
#include "chatPDU.h"
#include "ipConverter.h"

static const int BUFSIZE = 1000;
static const int PROGRAM_RUNNING_TIME = 100000000;
static const int ALIVE_MESSAGE_INTERVAL_IN_MICROSEC = 3000000;
static const int REG_MESSAGE_INTERVAL_IN_MICROSEC   = 1000000;
static const int NUMBER_OF_CONNECTED_CLIENTS_IS_NOT_A_CONSTANT = 1;
#define TRUE 1
#define FALSE 0
#define NS_REG 1
#define NS_NOT_REG 0

typedef struct{
	int sockFd;
	struct sockaddr_in addr;
	int id;
	pthread_t *thread;
} nameServer;


extern array *clients;
extern int serverPort;
extern int running;
extern char *serverName;

void setSocketTimeout(int sock, int seconds);

nameServer* initNameServer(char *hostname, int port);

void *startNameServerConnection(void *argument);

#endif /* UDP_UDP_H_ */
