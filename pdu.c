#include "pdu.h"

int PDU_read_allowed(PDU *pdu, int size) {
	if (pdu->ptr + size > pdu->size)
		return 0;
	return 1;
}

void PDU_reset(PDU *pdu) {
	pdu->ptr = 0;
}

pdu_byte PDU_read(PDU *pdu) {
	if (PDU_read_allowed(pdu, sizeof(pdu_byte))) {
		pdu_byte *i = (pdu_byte *) &pdu->pdu[pdu->ptr];
		pdu->ptr += sizeof(pdu_byte);
		return (pdu_byte) *i;
	}
	return -1;
}

PDU* PDU_create(pdu_int size) {
	PDU *pdu = malloc(sizeof(PDU));
	pdu->size = size;
	pdu->pdu = malloc(sizeof(pdu_byte) * size);
	return pdu;
}

PDU* PDU_createFromArray(char* arr, int size){
	PDU *pdu = malloc(sizeof(PDU));
	pdu->pdu = arr;
	pdu->size = size;
	pdu->ptr = 0;
	return pdu;
}

void PDU_append(PDU *pdu, pdu_byte *content, int size) {
	int newSize = pdu->size + size;
	pdu->pdu = realloc(pdu->pdu, newSize);
	for (int i = pdu->size; i < newSize; i++) {
		pdu->pdu[i] = content[i - pdu->size];
	}
	pdu->size = newSize;
	free(content);
}

void PDU_append_int(PDU *pdu, int i) {
	pdu_int *b = malloc(sizeof(pdu_int));
	*b = (pdu_int) i;
	*b = htonl(*b);
	PDU_append(pdu, (pdu_byte *) b, sizeof(pdu_int));
}

void PDU_append_short(PDU *pdu, int i) {
	pdu_short *b = malloc(sizeof(pdu_short));
	*b = (pdu_short) i;
	*b = htons(*b);
	PDU_append(pdu, (pdu_byte *) b, sizeof(pdu_short));
}

void PDU_append_byte(PDU *pdu, int i) {
	pdu_byte *b = malloc(sizeof(pdu_byte));
	*b = (pdu_byte) i;
	PDU_append(pdu, b, sizeof(pdu_byte));
}

void PDU_append_pad(PDU *pdu, int size) {
	for (int i = 0; i < size; i++) {
		PDU_append_byte(pdu, 0);
	}
}

int PDU_read_int(PDU *pdu) {
	if (PDU_read_allowed(pdu, sizeof(pdu_int))) {
		pdu_int *i = (pdu_int *) &pdu->pdu[pdu->ptr];
		pdu->ptr += sizeof(pdu_int);
		return (int) *i;
	}
	return -1;
}

int PDU_read_short(PDU *pdu) {
	if (PDU_read_allowed(pdu, sizeof(pdu_short))) {
		pdu_short *i = (pdu_short *) &pdu->pdu[pdu->ptr];
		pdu->ptr += sizeof(pdu_short);
		return (int) *i;
	}
	return -1;
}

int PDU_read_byte(PDU *pdu) {
	if (PDU_read_allowed(pdu, sizeof(pdu_byte))) {
		pdu_byte *i = (pdu_byte *) &pdu->pdu[pdu->ptr];
		pdu->ptr += sizeof(pdu_byte);
		return (int) *i;
	}
	return -1;
}

char *PDU_read_string(PDU *pdu, int length) {
	//length++; //null termination
	if (PDU_read_allowed(pdu, sizeof(char) * length)) {
		char *s = malloc(sizeof(char) * length);
		memcpy(s, &pdu->pdu[pdu->ptr], length);
		pdu->ptr += strlen(s);
		pdu->ptr += 4 - strlen(s) % 4;
		return s;
	}
	return (char*)-1;
}

int PDU_append_string_raw(PDU *pdu, char *s, int length) {
	int i = 0;
	for (i = 0; i < length; i++) {
		PDU_append_byte(pdu, (int) s[i]);
	}
	return i;
}
void PDU_append_pad_to_row(PDU *pdu){
	int i  = pdu->size;
	if(i % 4 == 0)
		return;
	for (int j = i % 4; j < 4; j++) {
			PDU_append_byte(pdu, (int) '\0');
		}

}
void PDU_append_fixed_string(PDU *pdu, char *s, int length) {
	PDU_append_string_raw(pdu, s, length);
	PDU_append_pad_to_row(pdu);
}

void PDU_append_string(PDU *pdu, char *s) {
	PDU_append_fixed_string(pdu, s, strlen(s));
}

int PDU_read_pad(PDU *pdu) {
	int pad = PDU_read_byte(pdu);
	if (pad != 0)
		printf("error in pad...");
	return pad;
}

int PDU_read_pads(PDU *pdu, int length) {
	int sum = 0;
	for (int i = 0; i < length; i++) {
		sum += PDU_read_pad(pdu);
	}
	return sum;
}

void PDU_free(PDU *pdu) {
	free(pdu->pdu);
	free(pdu);
}
