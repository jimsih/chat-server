#ifndef IPCONVERTER_H_
#define IPCONVERTER_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <unistd.h>

int string2ip(char *s);
char *ip2string(int ip);
char *hostname2string(char* hostname);
uint32_t hostname2ip(char* hostname);
char *getMyIp();

#endif /* IPCONVERTER_H_ */
