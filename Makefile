CC=gcc
CFLAGS= -Wall -std=c99 -g
objects = server.o pdu.o chatPDU.o ipConverter.o clientArray.o array.o checksum.o udp.o sighant.o


#DEPS = opCodes.h limits.h 

#%o: %.c $(DEPS)
#       $(CC) -Wall -std=c99 -c -o $@ $< $(CFLAGS)

server : $(objects)
	cc -o server -pthread $(objects)

server.o : opCodes.h pdu.h limits.h chatPDU.h clientArray.h checksum.h udp.h
sighant.o : sighant.h
chatPDU.o : opCodes.h limits.h ipConverter.h pdu.h
ipConverter.o : ipConverter.h
	gcc -Wall -c -g ipConverter.c
pdu.o : pdu.h
clientArray.o : limits.h array.h
array.o : array.h
checksum.o : checksum.h
udp.o : udp.h
	gcc -Wall -c -g udp.c


clean:
	rm -f *.o core
