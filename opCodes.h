#ifndef OP_CODES_H_
#define OP_CODES_H_

#define REG 0
#define ACK 1
#define ALIVE 2
#define GETLIST 3
#define SLIST 4
#define NOTREG 100
#define UNKNOWNOP 101
#define MESSAGE 10
#define QUIT 11
#define JOIN 12
#define CHNICK 13
#define UJOIN 16
#define ULEAVE 17
#define UCNICK 18
#define NICKS 19

#endif
