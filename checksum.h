#ifndef CHECKSUM_H_
#define CHECKSUM_H_

char checksum_calc(char *buf, int length);

void checksum_set(char *buf, int length, int index);

#endif /* CHECKSUM_H_ */
