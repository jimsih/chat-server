#ifndef CLIENTARRAY_H_
#define CLIENTARRAY_H_

#include <netinet/in.h>
#include <pthread.h>
#include <regex.h>
#include <sys/types.h>
#include <unistd.h>

#include "array.h"
#include "limits.h"

typedef struct {
	char *nicks;
	int num_nicks;
	int len;
} nicknames;

typedef struct {
	int id;
	int socket;
	char *nick;
	struct sockaddr * sock_addr;
	socklen_t* sock_len;
	pthread_t *thread;
} client;

extern pthread_mutex_t clientList_lock;

array* clients_create();
void clients_setNick(array *clis, client *cli, char *nick);
void clients_cnick(array *clis, int index, char *newNick);

int clients_nick_is_occupied(array *clis, char *nick);

int clients_add(array *clis, client *cli, char *nick);

void clients_remove(array *clis, client *cli);

client* clients_get(array *clis, int index);

void modifyNickname(array *clis, char *nick);

void modifyHelpFunc(char *nick);

#endif /* CLIENTARRAY_H_ */
