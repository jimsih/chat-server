#ifndef CHATPDU_H_
#define CHATPDU_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "opCodes.h"
#include "limits.h"
#include "ipConverter.h"
#include "pdu.h"
#include "checksum.h"

PDU* PDU_ACK(int id);
void PDU_READ_ACK(PDU* pdu);
void PDU_READ_NOTREG(PDU *pdu);
PDU *PDU_ALIVE(int clients, int id);
PDU *PDU_REG(int port, char* host, char *hostname);
PDU *PDU_UJOIN(char *nick);
PDU *PDU_ULEAVE(char *nick);
PDU *PDU_NICKS(int names, char* nicks, int length);
void nicksTest();
PDU *PDU_UCNICK(char *old, char *new);
PDU* PDU_MESSAGE(int type, char* message, int mess_len, char* nickname);
PDU * PDU_QUIT();
int PDU_READ_INCOMING(int socket, char* buffer);
int PDU_READ_JOIN(int socket, char* buffer);
int PDU_READ_MESSAGE(int socket, char* buffer);
int PDU_READ_QUIT(int socket, char* buffer);
int PDU_READ_CHNICK(int socket, char* buffer);

#endif /* CHATPDU_H_ */
