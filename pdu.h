#ifndef PDU_H_
#define PDU_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>

typedef unsigned char pdu_byte;
typedef unsigned short pdu_short;
typedef unsigned int pdu_int;

typedef struct {
	int size;
	char* pdu;
	int ptr;
} PDU;

void PDU_append_pad_to_row(PDU *pdu);
int PDU_read_allowed(PDU *pdu, int size);
void PDU_reset(PDU *pdu);
pdu_byte PDU_read(PDU *pdu);
PDU* PDU_create(pdu_int size);
PDU* PDU_createFromArray(char* arr, int size);
void PDU_append(PDU *pdu, pdu_byte *content, int size);
void PDU_append_int(PDU *pdu, int i);
void PDU_append_short(PDU *pdu, int i);
void PDU_append_byte(PDU *pdu, int i);
void PDU_append_pad(PDU *pdu, int size);
int PDU_read_int(PDU *pdu);
int PDU_read_short(PDU *pdu);
int PDU_read_byte(PDU *pdu);
char *PDU_read_string(PDU *pdu, int length);
int PDU_append_string_raw(PDU *pdu, char *s, int length);
void PDU_append_fixed_string(PDU *pdu, char *s, int length);
void PDU_append_string(PDU *pdu, char *s);
int PDU_read_pad(PDU *pdu);
int PDU_read_pads(PDU *pdu, int length);
void PDU_free(PDU *pdu);

#endif /* PDU_H_ */
