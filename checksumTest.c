#include "checkSum.h"
#include "strings.h"
#include <stdio.h>


int main() {
	char message[100];

	strcpy(message, "tsaddasaasdest");

	checksum_set(message, strlen(message), 0);

	char sum = checksum_calc(message, strlen(message));

	printf("[%d]", (int)sum); // should print []

	return (0);
}
