/**
 * Calculate checksum for a char array of size length
 */
char checksum_calc(char *buf, int length) {
	int sum = 0;
	int i = 0;

	while ((length--) != 0) {
		sum += (buf[i] & 0x000000FF);
		i++;
		if ((sum & 0x00000100) != 0) {
			sum &= 0x000000FF;
			sum++;
		}
	}

	return (char) ~(sum & 0xFF);
}

void checksum_set(char *buf, int length, int index) {
	buf[index] = 0;
	char sum = checksum_calc(buf, length);
	buf[index] = sum;
}

