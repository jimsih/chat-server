#include "chatPDU.h"

/**
 * NAMESERVER PACKETS
 */

PDU* PDU_ACK(int id) {
	PDU * pdu = PDU_create(0);
	PDU_append_byte(pdu, ACK);
	PDU_append_pad(pdu, 1);
	PDU_append_short(pdu, id);
	return pdu;
}

void PDU_READ_ACK(PDU* pdu) {
	PDU_reset(pdu);
	int op = PDU_read_byte(pdu);
	int pad = PDU_read_pad(pdu);
	int id = PDU_read_short(pdu);
	int len = pdu->size;
	printf("%d, %d, %d, %d", op, pad, id, len);
}

// NOTREG: READ
void PDU_READ_NOTREG(PDU *pdu) {
	PDU_reset(pdu);
	PDU_read_byte(pdu);
	PDU_read_pads(pdu, 3);
}

// ALIVE: SEND
PDU *PDU_ALIVE(int clients, int id) {
	PDU *pdu = PDU_create(0);
	PDU_append_byte(pdu, ALIVE);
	PDU_append_byte(pdu, clients);
	PDU_append_short(pdu, id);
	return pdu;
}

//REG: send
PDU *PDU_REG(int port, char* host, char *hostname) {
	PDU *pdu = PDU_create(0);
	PDU_append_byte(pdu, REG);
	PDU_append_byte(pdu, strlen(hostname));
	PDU_append_short(pdu, port);
	PDU_append_int(pdu, string2ip(host));
	PDU_append_string(pdu, hostname);
	return pdu;
}

//UJOIN
PDU *PDU_UJOIN(char *nick) {
	PDU *pdu = PDU_create(0);
	PDU_append_byte(pdu, UJOIN);
	PDU_append_byte(pdu, strlen(nick));
	PDU_append_pad(pdu, 2);

	int t = (int) time(NULL);
	PDU_append_int(pdu, t);
	PDU_append_string(pdu, nick);
	PDU_append_pad_to_row(pdu);

//	printf("UJOIN\n");
//	for (int i = 0; i<8; i+=4) {
//		printf("%d %d %d %d \n", pdu->pdu[i+0], pdu->pdu[i+1], pdu->pdu[i+2], pdu->pdu[i+3]);
//	}
//	for (int i = 8; i < pdu->size; i++) {
//		printf("%c ", pdu->pdu[i]);
//		if (i % 4 == 3) {
//			printf("\n");
//		}
//	}

	printf("Ujoin length: %d\n", pdu->size);
	return pdu;
}

//ULEAVE
PDU *PDU_ULEAVE(char *nick) {
	PDU *pdu = PDU_create(0);
	PDU_append_byte(pdu, ULEAVE);
	PDU_append_byte(pdu, strlen(nick));
	PDU_append_pad(pdu, 2);

	int t = (int) time(NULL);
	PDU_append_int(pdu, t);
	PDU_append_string(pdu, nick);
	PDU_append_pad_to_row(pdu);
	printf("Uleave length: %d\n", pdu->size);
	return pdu;
}

//NICKS
PDU *PDU_NICKS(int names, char* nicks, int length) {

	PDU *pdu = PDU_create(0);
	PDU_append_byte(pdu, NICKS);
	PDU_append_byte(pdu, names);
	PDU_append_short(pdu, length);
	PDU_append_fixed_string(pdu, nicks, length);
	PDU_append_pad_to_row(pdu);
//	printf("NICKS \n");
//	for (int i = 0; i<1; i++) {
//		printf("%d %d %d %d \n", pdu->pdu[i+0], pdu->pdu[i+1], pdu->pdu[i+2], pdu->pdu[i+3]);
//	}
//	for (int i = 4; i < pdu->size; i++) {
//		printf("%c ", pdu->pdu[i]);
//		if (i % 4 == 3) {
//			printf("\n");
//		}
//	}
	printf("nicks length: %d\n", pdu->size);
	return pdu;
}

//UCHNICK
PDU *PDU_UCNICK(char *old, char *new) {
	PDU *pdu = PDU_create(0);
	PDU_append_byte(pdu, UCNICK);
	PDU_append_byte(pdu, strlen(old));
	PDU_append_byte(pdu, strlen(new));
	PDU_append_pad(pdu, 1);

	int t = (int) time(NULL);
	PDU_append_int(pdu, t);
	PDU_append_string(pdu, old);
	PDU_append_string(pdu, new);
	printf("Uchnick length: %d\n", pdu->size);
	return pdu;
}

//MESS
PDU* PDU_MESSAGE(int type, char* message, int mess_len, char* nickname) {
	PDU* pdu = PDU_create(0);
	PDU_append_byte(pdu, MESSAGE);
	PDU_append_byte(pdu, type);

	// Error messages sends no nickname
	if (strlen(nickname) == 0)
		PDU_append_byte(pdu, 0);
	else
		PDU_append_byte(pdu, strlen(nickname)+1);

	PDU_append_byte(pdu, 0);
	PDU_append_short(pdu, mess_len);
	PDU_append_pad(pdu, 2);

	int t = (int) time(NULL);
	PDU_append_int(pdu, t);

	PDU_append_fixed_string(pdu, message, mess_len);
	PDU_append_fixed_string(pdu, nickname, strlen(nickname) + 1);
	checksum_set(pdu->pdu, pdu->size, 3);
	printf("Message \n");
	for (int i=0; i<pdu->size;i+=4) {
		printf("%d %d %d %d \n", pdu->pdu[i+0], pdu->pdu[i+1], pdu->pdu[i+2], pdu->pdu[i+3]);
	}
	printf("mess length: %d\n", pdu->size);
	return pdu;
}

//void PDU_READ_MESSAGE(PDU* pdu) {
//	PDU_reset(pdu);
//	int op = PDU_read_byte(pdu);
//	int type = PDU_read_byte(pdu);
//	int nickLen = PDU_read_byte(pdu);
//	int checkSum = PDU_read_byte(pdu);
//
//	int messLen = PDU_read_short(pdu);
//	PDU_read_pad(pdu);
//	PDU_read_pad(pdu);
//
//	int time = PDU_read_int(pdu);
//
//	char *mess = PDU_read_string(pdu, messLen);
//	char *nick = PDU_read_string(pdu, nickLen);
//
//	printf("\n%s: %s %d %d %d %d %d\n", nick, mess, op, type, nickLen, checkSum,
//			time);
//
//}

//QUIT
PDU * PDU_QUIT() {
	PDU *pdu = PDU_create(0);
	PDU_append_byte(pdu, QUIT);
	PDU_append_pad(pdu, 3);
	return pdu;
}

//void PDU_READ_QUIT(PDU* pdu) {
//	PDU_reset(pdu);
//	int op = PDU_read_byte(pdu);
//	int pad1 = PDU_read_pad(pdu);
//	int pad2 = PDU_read_pad(pdu);
//	int pad3 = PDU_read_pad(pdu);
//	if (pad1 || pad2 || pad3)
//		printf("error pad");
//	printf("Op-code: %d (expected: %d)", op, QUIT);
//
//}

int PDU_READ_INCOMING(int socket, char* buffer) {
	char mesg_OP[1];
	int pdu_len = 0;

	pdu_len = recv(socket, mesg_OP, 1, MSG_WAITALL);
	if (pdu_len == -1)  {
		perror("recv: ");
		return -1;
	}

	printf("OP: %d \n", mesg_OP[0]);
	switch (mesg_OP[0]) {
	case MESSAGE:
		return PDU_READ_MESSAGE(socket, buffer);
		break;
	case QUIT:
		return PDU_READ_QUIT(socket, buffer);
		break;
	case JOIN:
		return PDU_READ_JOIN(socket, buffer);
		break;
	case CHNICK:
		return PDU_READ_CHNICK(socket, buffer);
		break;
	}

	return -1;
}

int PDU_READ_JOIN(int socket, char* buffer) {
	char header[3];
	char nick[NICKNAME_LIMIT+1];
	int pdu_len = 0;
	int pdu_ptr = 0;

	buffer[pdu_ptr] = JOIN;
	pdu_ptr++;

	pdu_len = recv(socket, header, 3, MSG_WAITALL);
	if (pdu_len == -1 || pdu_len < 3)
		return -1;

	for (int i=0; i<3; i++) {
		buffer[pdu_ptr] = header[i];
		pdu_ptr++;
	}

	int nick_len = (int)(unsigned char) header[0];
	int pad = (nick_len % 4);
	if (pad != 0)
		pad = 4 - pad;
	pdu_len = recv(socket, nick, nick_len+pad, MSG_WAITALL);
	if (pdu_len == -1 || pdu_len < nick_len+pad)
		return -1;

	for (int i=0; i<nick_len; i++) {
		buffer[pdu_ptr] = nick[i];
		pdu_ptr++;
	}

	buffer[pdu_ptr] = '\0';

	printf("join_buffer \n");
	for (int i=0; i<nick_len+4;i+=4) {
			printf("%d %d %d %d \n", buffer[i+0], buffer[i+1], buffer[i+2], buffer[i+3]);
	}

	return pdu_ptr;
}

int PDU_READ_MESSAGE(int socket, char* buffer) {
	char header[11];
	char mess[MESSAGE_LIMIT];
	int pdu_len = 0;
	int pdu_ptr = 0;

	buffer[pdu_ptr] = MESSAGE;
	pdu_ptr++;

	pdu_len = recv(socket, header, 11, MSG_WAITALL);
	if (pdu_len == -1 || pdu_len < 11)
		return -1;
	if (header[0] < 0 || header[0] > 3)
		return -1;

	for (int i=0; i<11; i++) {
		buffer[pdu_ptr] = header[i];
		pdu_ptr++;
	}

	int mess_len = (int)(header[3] << 8) | header[4];
	int pad = mess_len % 4;
	if (pad != 0)
		pad = 4 - pad;
	pdu_len = recv(socket, mess, mess_len+pad, MSG_WAITALL);
	if (pdu_len == -1 || pdu_len < mess_len+pad)
		return -1;

	for (int i=0; i<mess_len; i++) {
		buffer[pdu_ptr] = mess[i];
		pdu_ptr++;
	}

	buffer[pdu_ptr] = '\0';

	printf("message_buffer \n");
	for (int i=0; i<mess_len+12;i+=4) {
			printf("%d %d %d %d \n", buffer[i+0], buffer[i+1], buffer[i+2], buffer[i+3]);
	}

	return pdu_ptr;
}

int PDU_READ_QUIT(int socket, char* buffer) {
	char header[3];
	int pdu_len = 0;
	int pdu_ptr = 0;

	buffer[pdu_ptr] = QUIT;
	pdu_ptr++;

	pdu_len = recv(socket, header, 3, MSG_WAITALL);
	if (pdu_len == -1 || pdu_len < 3)
		return -1;

	printf("quit_buffer \n%d %d %d %d \n", buffer[0],header[0], header[1], header[2]);
	for (int i=0; i<3; i++) {
		buffer[pdu_ptr] = header[i];
		pdu_ptr++;
	}

	return pdu_ptr;
}

int PDU_READ_CHNICK(int socket, char* buffer) {
	char header[3];
	char nick[NICKNAME_LIMIT+1];
	int pdu_len = 0;
	int pdu_ptr = 0;

	buffer[pdu_ptr] = CHNICK;
	pdu_ptr++;

	pdu_len = recv(socket, header, 3, MSG_WAITALL);
	if (pdu_len == -1 || pdu_len < 3)
		return -1;

	for (int i=0; i<3; i++) {
		buffer[pdu_ptr] = header[i];
		pdu_ptr++;
	}

	int nick_len = (int)(unsigned char) header[0];
	int pad = (nick_len % 4);
	if (pad != 0)
		pad = 4 - pad;
	pdu_len = recv(socket, nick, nick_len+pad, MSG_WAITALL);
	if (pdu_len == -1 || pdu_len < nick_len+pad)
		return -1;

	for (int i=0; i<nick_len; i++) {
		buffer[pdu_ptr] = nick[i];
		pdu_ptr++;
	}

	buffer[pdu_ptr] = '\0';

	printf("chnick_buffer \n");
	for (int i=0; i<nick_len+4;i+=4) {
		printf("%d %d %d %d \n", buffer[i+0], buffer[i+1], buffer[i+2], buffer[i+3]);
	}

	return pdu_ptr;
}

