#include "udp.h"



int ns_state = NS_NOT_REG;

void setSocketTimeout(int sock, int seconds) {
	struct timeval tv;
	tv.tv_sec = seconds;
	tv.tv_usec = 0;
	if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
		perror("Error");
	}
}

nameServer* initNameServer(char *nameserver, int nameserverPort) {

	nameServer *ns = malloc(sizeof(nameServer));
	ns->sockFd = socket(AF_INET, SOCK_DGRAM, 0);

	setSocketTimeout(ns->sockFd, 1);

	bzero(&(ns->addr), sizeof(ns->addr));
	ns->addr.sin_family = AF_INET;

	ns->addr.sin_addr.s_addr = hostname2ip(nameserver);
	ns->addr.sin_port = htons(nameserverPort);

	return ns;
}

void registerToNs(nameServer *ns) {
	usleep(REG_MESSAGE_INTERVAL_IN_MICROSEC);

	//TODO: convert "my" ip address to correct ip address. Now it is hard coded...

	char *ip = getMyIp();

	PDU* pdu = PDU_REG(serverPort, ip, serverName);

	sendto(ns->sockFd, pdu->pdu, pdu->size, 0, (struct sockaddr *) &(ns->addr),
			sizeof(ns->addr));

	PDU_free(pdu);

}

void sendAliveMessageToServer(nameServer *ns) {

	usleep(ALIVE_MESSAGE_INTERVAL_IN_MICROSEC);

	PDU* pdu = PDU_ALIVE(clients->numElements, ns->id);
	sendto(ns->sockFd, pdu->pdu, pdu->size, 0, (struct sockaddr *) &(ns->addr),
			sizeof(ns->addr));

	PDU_free(pdu);
}

void *startNameServerConnection(void *argument) {
	nameServer *ns = (nameServer*) argument;
	int op, n, read_id;
	char recvline[BUFSIZE];
	PDU* pdu;

	while(running) {
		if ((n = recvfrom(ns->sockFd, recvline, BUFSIZE, 0, NULL, NULL)) > 0) {
			pdu = PDU_createFromArray(recvline, n);
			op = PDU_read_byte(pdu);
			PDU_read_pad(pdu);
			read_id = ntohs(PDU_read_short(pdu));
			ns->id = read_id;
			free(pdu);
			if (op == ACK) {
				fprintf(stderr, "ACK RECEIVED\n");
				ns_state = NS_REG;
			} else if (op == NOTREG) {
				ns_state = NS_NOT_REG;
				//fprintf(stderr, "NOT REG\n");
			} else {
				fprintf(stderr, "OP not known \n");
			}

		}
		if (ns_state == NS_NOT_REG) {
			registerToNs(ns);
		} else if (ns_state == NS_REG) {
			sendAliveMessageToServer(ns);
		}
	}
	return NULL;
}

