
#ifndef ARRAY_H_
#define ARRAY_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef void * data;

typedef struct {
	data *d;
	int size;
	int numElements;
} array;

int array_valid_index(array *a, int index);
array *array_create(int length);
void array_add(array *a, int index, data d);
data *array_get(array *a, int index);
void array_remove(array *a, int index);
int array_get_free_index(array *a);
void array_free(array *a);

#endif /* ARRAY_H_ */
